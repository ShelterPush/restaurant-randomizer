# Script designed to aid in decided what to eat.
import random
import sys # only included for sys.exit()

cheap = ['McDonalds', 'Subway', "Little Caesar's"]
notcheap = ["O'Charleys", 'Chilis', "Moe's"]
either = cheap + notcheap
takeoutonly = ['McDonalds', 'Subway', "Little Caesar's"]

while True: # outer loop
    while True: # determine if in or out
        print("Dine in or take out? Type 'in' or 'out'")
        takeoutoreatin = input()
        if takeoutoreatin == 'out':
            foodlist = takeoutonly
            break # takes me to the foodlist shuffling portion
        elif takeoutoreatin == 'in':
            while True: # extra options for eating in
                print("Cheap, not as cheap, or either? Type 'cheap', 'notcheap', or 'either'")
                cheapornot = input()
                if cheapornot == 'cheap':
                    foodlist = cheap
                    break # only takes me back to the 'elif' portion, so I'll have to 'break' again
                elif cheapornot == 'notcheap':
                    foodlist = notcheap
                    break
                elif cheapornot == 'either':
                    foodlist = either
                    break
                else:
                    print("Please enter 'cheap', 'notcheap', or 'either' (without apostrophes or commas)")
                    continue
            break # takes me to the foodlist shuffling portion
        else: # error checking
           print("Please enter 'in' or 'out' (without apostrophes)")
    random.shuffle(foodlist)
    if foodlist == []: # checks if the list is empty (because .pop() deletes each entry it shows) before it tries to show something
        print()
        print('You have seen all available restaurants for the options given.')
        print("If you would like to see them again, please choose 'no' and reboot the script.")
    else:
        print()
        print('You should try',foodlist.pop())
    while True: # determine if continue suggesting food
        print("Would you like another suggestion? Type 'yes' or 'no'")
        goodenough = input()
        if goodenough == 'no':
            print()
            input('Press the Enter key to close this window')
            sys.exit() # just 'break'ing out of this would start me right back at the beginning of the loop, so I sys.exit()
        elif goodenough == 'yes':
            break
        else:
            print("Please enter 'yes' or 'no' (without apostrophes)")
